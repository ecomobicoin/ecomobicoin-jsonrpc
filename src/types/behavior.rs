use crate::prelude::*;
use serde_with::{DeserializeFromStr, SerializeDisplay};

/// Macro used by MessageCall types (LegacyType, EIP2930Type, EIP1155Type)
///
/// It implements `Display` and `FromStr` to convert to/from the market to the string of its type.
///
/// The MessageCallTypes must implement the associated constant `TYPE`.
macro_rules! impl_display_and_from_str_for_type {
    ($ty:ty) => {
        impl std::fmt::Display for $ty {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(f, "{}", <$ty>::TYPE)
            }
        }

        impl std::str::FromStr for $ty {
            type Err = String;

            fn from_str(s: &str) -> Result<Self, Self::Err> {
                if s == <$ty>::TYPE {
                    Ok(<$ty>::default())
                } else {
                    Err(format!("Invalid type {}, expected {}", s, <$ty>::TYPE))
                }
            }
        }
    };
}

#[derive(PartialEq, Eq, Debug, Copy, Clone, Default, DeserializeFromStr, SerializeDisplay)]
pub struct QuantifiableType;
impl QuantifiableType {
    const TYPE: &'static str = "0x00";
}
impl_display_and_from_str_for_type!(QuantifiableType);

#[serde_as]
#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]
#[serde(untagged, deny_unknown_fields)]
pub enum BehaviorCall {
    #[serde(rename_all = "camelCase")]
    Quantifiable {
        #[serde(rename = "type", default, skip_serializing_if = "Option::is_none")]
        tag: Option<QuantifiableType>,
        #[serde(default, skip_serializing_if = "Option::is_none")]
        from: Option<Address>,
        #[serde(default, skip_serializing_if = "Option::is_none")]
        to: Option<Address>,
        // #[serde(default, skip_serializing_if = "Option::is_none")]
        // gas: Option<U64>,
        // #[serde(default, skip_serializing_if = "Option::is_none")]
        // gas_price: Option<U256>,
        quantity: U256,
        timestamp: U64,
        input: Bytes,
        // #[serde(default, skip_serializing_if = "Option::is_none")]
        // access_list: Option<Vec<AccessListEntry>>,
    },
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(deny_unknown_fields, rename_all = "camelCase")]
pub struct BehaviorMessage {
    pub chain_id: U64,
    // nonce: U64,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub to: Option<Address>,
    pub quantity: U256,
    pub timestamp: U64,
    pub input: Bytes,
    // access_list: Vec<AccessListEntry>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Behavior {
    #[serde(flatten)]
    pub message: BehaviorMessage,
    /// RLP encoded representation of the transaction.
    pub v: U64,
    pub r: H256,
    pub s: H256,

    pub from: Address,
    pub hash: H256,
    pub behavior_index: Option<U64>,
    pub block_number: Option<U64>,
    pub block_hash: Option<H256>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(untagged)]
/// Tx is either a transaction or a transaction hash.
pub enum Bx {
    /// Behavior.
    Behavior(Box<Behavior>),
    /// Transaction hash.
    Hash(H256),
}

#[cfg(test)]
mod tests {
    use super::*;
    use hex_literal::hex;
    use serde_json::json;

    #[test]
    fn test_ser_de_hexbytes_option() {
        let call_data = BehaviorCall::Quantifiable {
            tag: None,
            from: None,
            to: None,
            quantity: U256::from(10_u64),
            timestamp: 1661173680u64.into(),
            input: Bytes::from(&b"LIMOS"[..]),
        };
        let hexstring = json!({
            "input":"0x4c494d4f53",
            "quantity":"0xa",
            "timestamp":"0x63037fb0",
        });
        assert_eq!(serde_json::to_value(&call_data).unwrap(), hexstring);
        assert_eq!(
            serde_json::from_value::<BehaviorCall>(hexstring).unwrap(),
            call_data
        );
    }

    #[test]
    fn test_deserialize_with_tag() {
        let call_data = BehaviorCall::Quantifiable {
            tag: None,
            from: Some(Address::from([0; 20])),
            to: Some(Address::from([0; 20])),
            quantity: U256::from(10_u64),
            timestamp: 1661173680u64.into(),
            input: Bytes::from(&b"LIMOS"[..]),
        };
        let hexstring = json!({
            "from":"0x0000000000000000000000000000000000000000",
            "to":"0x0000000000000000000000000000000000000000",
            "input":"0x4c494d4f53",
            "quantity":"0xa",
            "timestamp":"0x63037fb0",
        });

        assert_eq!(
            serde_json::from_value::<BehaviorCall>(hexstring).unwrap(),
            call_data,
        );

        let call_data_with_tag = BehaviorCall::Quantifiable {
            tag: Some(QuantifiableType),
            from: Some(Address::from([0; 20])),
            to: Some(Address::from([0; 20])),
            quantity: U256::from(10_u64),
            timestamp: 1661173680u64.into(),
            input: Bytes::from(&b"LIMOS"[..]),
        };
        let hexstring_with_tag: serde_json::Value = json!({
            "type": "0x00",
            "from":"0x0000000000000000000000000000000000000000",
            "to":"0x0000000000000000000000000000000000000000",
            "input":"0x4c494d4f53",
            "quantity":"0xa",
            "timestamp":"0x63037fb0",
        });

        assert_eq!(
            serde_json::from_value::<BehaviorCall>(hexstring_with_tag).unwrap(),
            call_data_with_tag,
        );
    }

    #[test]
    fn test_deserialize_with_tag_fails() {
        let hexstring = json!({
            "type": "0xdeadbeef",
            "to":"0x0000000000000000000000000000000000000000",
        });

        assert_eq!(
            &serde_json::from_value::<BehaviorCall>(hexstring)
                .err()
                .unwrap()
                .to_string(),
            "data did not match any variant of untagged enum BehaviorCall"
        );
    }

    #[test]
    fn test_bx_ser() {
        let bx = Behavior {
            message: BehaviorMessage {
                chain_id: 2_u64.into(),
                quantity: U256::from(21000_u64),
                timestamp: 20_000_000_000_u64.into(),
                to: Some(hex!("727fc6a68321b754475c668a6abfb6e9e71c169a").into()),
                input: hex!("a9059cbb000000000213ed0f886efd100b67c7e4ec0a85a7d20dc971600000000000000000000015af1d78b58c4000").to_vec().into(),
            },
            v: 40_u64.into(),
            r: hex!("be67e0a07db67da8d446f76add590e54b6e92cb6b8f9835aeb67540579a27717").into(),
            s: hex!("2d690516512020171c1ec870f6ff45398cc8609250326be89915fb538e7bd718").into(),
            from: Address::repeat_byte(0xAA),
            hash: H256::repeat_byte(0xBB),
            behavior_index: Some(0x42.into()),
            block_hash: None,
            block_number: None,
        };
        let serialized = json!({
            "chainId": "0x2",
            "to": "0x727fc6a68321b754475c668a6abfb6e9e71c169a",
            "timestamp":"0x4a817c800",
            "quantity": "0x5208",
            "input":"0xa9059cbb000000000213ed0f886efd100b67c7e4ec0a85a7d20dc971600000000000000000000015af1d78b58c4000",
            "v":"0x28",
            "r":"0xbe67e0a07db67da8d446f76add590e54b6e92cb6b8f9835aeb67540579a27717",
            "s":"0x2d690516512020171c1ec870f6ff45398cc8609250326be89915fb538e7bd718",
            "from":"0xaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            "hash":"0xbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
            "behaviorIndex":"0x42",
            "blockHash": null,
            "blockNumber": null,
        });

        assert_eq!(serde_json::to_value(&bx).unwrap(), serialized);
        assert_eq!(serde_json::from_value::<Behavior>(serialized).unwrap(), bx);
    }
}
