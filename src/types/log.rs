use crate::prelude::*;

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
/// Transaction's log entry.
pub struct TransactionLog {
    /// Log's index within transaction.
    pub log_index: Option<U64>,
    /// Transaction's index within block.
    pub transaction_index: Option<U64>,
    /// Transaction's hash.
    pub transaction_hash: Option<H256>,
    /// Block's hash, transaction is included in.
    pub block_hash: Option<H256>,
    /// Block number, transaction is included in.
    pub block_number: Option<U64>,
    /// Log's address.
    pub address: Address,
    /// Log's data.
    pub data: Bytes,
    /// Log's Topics.
    pub topics: Vec<H256>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
/// Behavior's log entry.
pub struct BehaviorLog {
    /// Log's index within behavior.
    pub log_index: Option<U64>,
    /// Behavior's index within block.
    pub behavior_index: Option<U64>,
    /// Behavior's hash.
    pub behavior_hash: Option<H256>,
    /// Block's hash, behavior is included in.
    pub block_hash: Option<H256>,
    /// Block number, behavior is included in.
    pub block_number: Option<U64>,
    /// Log's address.
    pub address: Address,
    /// Log's data.
    pub data: Bytes,
    /// Log's Topics.
    pub topics: Vec<H256>,
}
